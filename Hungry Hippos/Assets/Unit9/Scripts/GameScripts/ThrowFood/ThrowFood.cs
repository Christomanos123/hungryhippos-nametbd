﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowFood : MonoBehaviour {
    //Unparent the cube
    //Physics calculations in fixed update
    //Make it so depending on the swipe it should move to either to the left or right
    // 



    float touchStartTime, touchEndTime, swipeDistance, swipeTime;
    private Vector2 startPos, endPos;
    [SerializeField] float minSwipeDistance = 0;
    [SerializeField] private int foodSpeedMultiplier = 1;
    private float foodVelocity = 0;
    private float foodSpeed = 0;
    private Vector3 angle;
    private Rigidbody rigidFood;
    private Vector3 initialPosition;
    private Quaternion initialRotation;
    private bool thrown, holding;
    private Vector3 newPosition;
    Camera arCamera;

   // [SerializeField] private float force,Upforce;
    //[SerializeField] private ForceMode forceMode;


   // Vector3 velocity;
    //[SerializeField] float velocityAmount;
    //[SerializeField] private float LaunchAngle;
    
    void Start () 
    {
        arCamera = Camera.main.GetComponent<Camera>();
    }
    
    
    void Update ()
    {
        rigidFood = FoodGenerator.Instance.m_Tray[0].GetComponent<Rigidbody>();
        initialPosition = FoodGenerator.Instance.m_Tray[0].transform.position;
        initialRotation = FoodGenerator.Instance.m_Tray[0].transform.rotation;
        if (holding)
        {
            OnTouch();
        }

       

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = arCamera.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;
                //Cast a ray from the camera
                if (Physics.Raycast(ray, out hit,100f))
                {
                    //If what we hit is the cubes's tranform
                    if (hit.transform == FoodGenerator.Instance.m_Tray[0].transform)
                    {
                        FoodGenerator.Instance.g_isLaunching = true;
                        rigidFood.isKinematic = false;
                        FoodGenerator.Instance.m_Tray[0].GetComponentInChildren<FloatingObject>().enabled = false;
                       
                        touchStartTime = Time.time;
                        startPos = touch.position;
                        holding = true;
                    }
                }

            }
            else if (touch.phase == TouchPhase.Ended && holding)
            {
                touchEndTime = Time.time;
                endPos = touch.position;
                NormalizeTouchPositions();
                swipeTime = touchEndTime - touchStartTime;
                //Debug.Log("swipeTime = " + swipeTime + " " + "swipeDistance = " + swipeDistance);
                //normalize the swipe distance, 
                if (swipeTime < 0.5f && swipeDistance > 0.2f)
                {
                    
                    CalculateSpeed();
                    CalculateAngle();
                    rigidFood.AddForce(angle * foodSpeed);
                    holding = false;
                    thrown = true;
                    StartCoroutine(AddtoListWithDelay());

                }
                else
                {
                    Reset();
                }
            }


            //if (touchStartTime > 0)
            //{
            //    tempTime = Time.time;
            //    if (tempTime > 0.5f)
            //    {
            //        touchStartTime = Time.time;
            //        startPos = touch.position;
            //    }
            //}
        }
    }

    private void FixedUpdate()
    {
        if (thrown)
        {
            AddForce();
        }
    }

    void AddForce()
    {
        CalculateSpeed();
        CalculateAngle();
        rigidFood.AddForce(angle * foodSpeed);
       // rigid.AddForce(transform.forward * force, forceMode);
       // rigid.AddForce(transform.up * Upforce, forceMode);
        FoodGenerator.Instance.m_Tray[0].transform.SetParent(null);
        rigidFood.useGravity = true;
        thrown = false;
    }

    void NormalizeTouchPositions()
    {
       //startPos.x = startPos.x / Screen.width;
       //startPos.y = startPos.y / Screen.height;

       // endPos.x = endPos.x / Screen.width;
       // endPos.y = endPos.y / Screen.height;

        swipeDistance = (endPos - startPos).magnitude;

    }


    IEnumerator AddtoListWithDelay()
    {
        yield return new WaitForSeconds(0.1f);
        FoodGenerator.Instance.m_Tray.RemoveAt(0);
        FoodGenerator.Instance.addToList();
        FoodGenerator.Instance.g_isLaunching = false;
    }


    void OnTouch()
    {
        FoodGenerator.Instance.m_Tray[0].transform.SetParent(null);
        Vector3 touchPos = Input.GetTouch(0).position;
        Ray ray = arCamera.ScreenPointToRay(touchPos);
        FoodGenerator.Instance.m_Tray[0].transform.localPosition = ray.origin + (ray.direction * 3);
        //touchPos.z = arCamera.nearClipPlane * 30.0f;
        //transform.localPosition = Vector3.Lerp(transform.position, newPosition, 80f * Time.deltaTime);
        FoodGenerator.Instance.m_Tray[0].transform.SetParent(arCamera.transform);

    }


    void CalculateSpeed()
    {
        if (swipeTime > 0)
        {
            foodVelocity = swipeDistance / swipeTime;
        }
        foodSpeed = foodVelocity * foodSpeedMultiplier;
        swipeTime = 0;
    }


    void CalculateAngle()
    {
        angle =  arCamera.ScreenToWorldPoint(new Vector3(endPos.x, endPos.y + 30f, (arCamera.nearClipPlane + 9.0f))) - arCamera.transform.position;
        angle.Normalize();
    }

    //Reset first item if not launched
    private void Reset()
    {
        Transform spawnPoint = FoodGenerator.Instance.m_SpawnPos.transform;
        FoodGenerator.Instance.m_Tray[0].transform.SetPositionAndRotation(spawnPoint.position, spawnPoint.rotation);
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        GetComponent<Rigidbody>().useGravity = false;
        FoodGenerator.Instance.m_Tray[0].GetComponentInChildren<FloatingObject>().enabled = true;
        FoodGenerator.Instance.m_Tray[0].GetComponent<Rigidbody>().isKinematic = true;
        thrown = holding = false;

    }




}
