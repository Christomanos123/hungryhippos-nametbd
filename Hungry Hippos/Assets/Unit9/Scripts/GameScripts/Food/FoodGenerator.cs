﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class FoodGenerator : MonoBehaviour {

    static FoodGenerator foodGenerator;
    public static FoodGenerator Instance
    {
        get
        {
            if (foodGenerator == null)
                foodGenerator = FindObjectOfType<FoodGenerator>();

            return foodGenerator;
        }
    }

    public bool g_isTrayRegenarated;
    public bool g_isLaunching;
    public bool g_isTrayReady;

    public List<GameObject> m_Tray = new List<GameObject>();
    public Dictionary<foodtypes, GameObject> m_TypesOfFood = new Dictionary<foodtypes, GameObject>();
    public List<foodtypes> m_DesiredFoodList = new List<foodtypes>();
    List<Animal> spawnedAnimalList; 

    public List<GameObject> m_BurgerStashList = new List<GameObject>();
    public List<GameObject> m_IceCreamStashList = new List<GameObject>();
    public List<GameObject> m_RottenFoodStashList = new List<GameObject>();

    [SerializeField] private GameObject m_IceCreamPrefab;
    [SerializeField] private GameObject m_BurgerPrefab;
    [SerializeField] private GameObject m_RottenFoodPrefab;

    [SerializeField] public Transform m_SpawnPos;
    [SerializeField] public Transform m_PreviewFoodPos;
    [SerializeField] public Transform m_RestOfFoodPos;

    [SerializeField]  GameObject arCamera;

    private const int LISTMAX = 10;

    void Start ()
    {
        g_isTrayReady = false;
        g_isTrayRegenarated = false;
        spawnedAnimalList = AnimalHandler.Instance.spawnedAnimalList;
        InitalizeTypesOfFood();

        if (spawnedAnimalList.Any())
        {

            for (int i = 0; i < m_TypesOfFood.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        InstanstiateFoodStash(m_IceCreamStashList, m_TypesOfFood[foodtypes.icecream]);
                        break;
                    case 1:
                        InstanstiateFoodStash(m_BurgerStashList, m_TypesOfFood[foodtypes.burger]);
                        break;
                    case 2:
                        InstanstiateFoodStash(m_RottenFoodStashList, m_TypesOfFood[foodtypes.rotten]);
                        break;
                    default:
                        break;
                }
            }

            GenerateDesiredFoodList();
            GenerateTrayList();
            g_isTrayReady = true;
        }

    }

    void Update()
    {
        if (GameController.Instance.currentState == GameController.GameStates.GameAR && AnimalHandler.Instance.g_repeatGame && g_isTrayRegenarated == false) 
        {
            GenerateDesiredFoodList();
            GenerateTrayList();
            g_isTrayRegenarated = true;
            g_isTrayReady = true;
        }


        if (m_Tray.Any() && GameController.Instance.currentState == GameController.GameStates.GameAR)
        {
            MoveFoodToPosition();
        }
    }

    public  void GenerateDesiredFoodList()
    {
        for (int i = 0; i < spawnedAnimalList.Count; i++)
        {
            if (!m_DesiredFoodList.Contains(spawnedAnimalList[i].desiredFood))
            {
                m_DesiredFoodList.Add(spawnedAnimalList[i].desiredFood);
            }
        }
    }

    public void InstanstiateFoodStash(List<GameObject> foodStashList, GameObject food)
    {
        for (int i = 0; i < LISTMAX + 40; i++)
        {
            GameObject spawnedFood = Instantiate(food, m_RestOfFoodPos.position, m_RestOfFoodPos.rotation);
            spawnedFood.transform.SetParent(arCamera.transform);
            spawnedFood.GetComponent<FoodBehaviour>().inTray = false;
            spawnedFood.GetComponent<FoodBehaviour>().inTransition = false;
            foodStashList.Add(spawnedFood);
        }
    }

    public void InitalizeTypesOfFood()
    {
        m_TypesOfFood.Add(foodtypes.icecream, m_IceCreamPrefab); //Elephant
        m_TypesOfFood.Add(foodtypes.burger, m_BurgerPrefab); //bear
        m_TypesOfFood.Add(foodtypes.rotten, m_RottenFoodPrefab);
    }

    public void GenerateTrayList()
    {
        for (int i = 0; i < LISTMAX; i++)
        {

            int randomFood = Random.Range(0, m_DesiredFoodList.Count);
     
            switch (m_DesiredFoodList[randomFood])
            {
                case foodtypes.icecream:
                    AddFoodStashToTray(m_IceCreamStashList);
                    break;

                case foodtypes.burger:
                    AddFoodStashToTray(m_BurgerStashList);
                    break;
                case foodtypes.rotten:
                    AddFoodStashToTray(m_RottenFoodStashList);
                    break;
                default:
                    break;
            }
        }
    }

    void AddFoodStashToTray(List<GameObject> foodStashList)
    {
        for (int i = 0; i < foodStashList.Count; i++)
        {
            bool inTray = foodStashList[i].GetComponent<FoodBehaviour>().inTray;
            if (inTray == false)
            {
                m_Tray.Add(foodStashList[i]);
                m_Tray[i].GetComponent<FoodBehaviour>().inTray = true;
                break;
            }
        }
    }


    public void addToList()
    {
        if (m_Tray.Count <= LISTMAX)
        {
            int randomFood = Random.Range(0, m_DesiredFoodList.Count);

            switch (m_DesiredFoodList[randomFood])
            {
                case foodtypes.burger:
                    AddFoodToEndOfTray(m_BurgerStashList);
                    break;
                case foodtypes.icecream:
                    AddFoodToEndOfTray(m_IceCreamStashList);
                    break;
                case foodtypes.rotten:
                    AddFoodToEndOfTray(m_RottenFoodStashList);
                    break;
                default:
                    break;
            }
        }
    }

    void AddFoodToEndOfTray(List<GameObject> foodStashList)
    {
        for (int i = 0; i < foodStashList.Count; i++)
        {
            bool inTray = foodStashList[i].GetComponent<FoodBehaviour>().inTray;
            bool inTransition = foodStashList[i].GetComponent<FoodBehaviour>().inTransition;

            if (inTray == false && inTransition == false)
            {
                m_Tray.Add(foodStashList[i]);
                m_Tray[m_Tray.Count - 1].GetComponent<FoodBehaviour>().inTray = true;
                m_Tray[m_Tray.Count - 1].GetComponent<TimeBasedDestroy>().enabled = false;
                m_Tray[m_Tray.Count - 1].transform.SetParent(arCamera.transform);
                inTray = true;
                break;
            }
        }
    }

    void MoveFoodToPosition()
    {
        //Spawn food ready for launch
        if (g_isLaunching == false)
        {
            m_Tray[0].transform.position = m_SpawnPos.position;
            m_Tray[0].transform.rotation = m_SpawnPos.rotation;
            m_Tray[0].transform.localScale = m_SpawnPos.localScale;
            m_Tray[0].GetComponentInChildren<FloatingObject>().enabled = true;
            m_Tray[0].GetComponent<BoxCollider>().enabled = true;
            m_Tray[0].GetComponent<Rigidbody>().isKinematic = true;   
        }

        //Spawn preview food
        m_Tray[1].transform.position = m_PreviewFoodPos.position;
        m_Tray[1].transform.rotation = m_PreviewFoodPos.transform.rotation;
        m_Tray[1].transform.localScale = m_PreviewFoodPos.localScale;

        for (int i = 2; i < LISTMAX; i++)
        {
            m_Tray[i].transform.position = m_RestOfFoodPos.transform.position;
            m_Tray[i].transform.rotation = m_RestOfFoodPos.transform.rotation;
            m_Tray[i].transform.localScale = m_RestOfFoodPos.localScale;
            m_Tray[i].GetComponent<Rigidbody>().isKinematic = true;
        }
    }


    public void Reset()
    {
        m_Tray.Clear();
        m_DesiredFoodList.Clear();
        g_isLaunching = false;
        ResetFoodStash(m_BurgerStashList);
        ResetFoodStash(m_IceCreamStashList);
        ResetFoodStash(m_RottenFoodStashList);
    }


    void ResetFoodStash(List<GameObject> foodStashList)
    {
        for (int i = 0; i < foodStashList.Count; i++)
        {
            foodStashList[i].GetComponentInChildren<FloatingObject>().enabled = false;
            foodStashList[i].GetComponent<BoxCollider>().enabled = false;
            foodStashList[i].GetComponent<FoodBehaviour>().inTray = false;
            foodStashList[i].GetComponent<Rigidbody>().isKinematic = true;
            foodStashList[i].transform.position = m_RestOfFoodPos.transform.position;
            foodStashList[i].transform.rotation = m_RestOfFoodPos.transform.rotation;
            foodStashList[i].transform.SetParent(arCamera.transform);
        }
    }


}
