﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBasedDestroy : MonoBehaviour 
{
    bool hasRun;

    private void OnEnable()
    {
        hasRun = false;
    }


    private void Update()
    {
        if (GetComponent<FoodBehaviour>().inTransition == true && hasRun == false)
        {
            StartCoroutine(ToggleInTransit());
            hasRun = true;
        }
    }

    IEnumerator ToggleInTransit()
    {
        yield return new WaitForSeconds(10f);
        GetComponent<FoodBehaviour>().inTransition = false;
        GetComponent<TimeBasedDestroy>().enabled = false;
    }
}
