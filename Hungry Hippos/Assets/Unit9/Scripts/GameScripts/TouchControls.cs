﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.XR.iOS;
using UnityEngine;

public class TouchControls : MonoBehaviour {

    static TouchControls touchControls;
    public static TouchControls Instance
    {
        get
        {
            if (touchControls == null)
                touchControls = GameObject.FindObjectOfType<TouchControls>();

            return touchControls;
        }
    }

    public GameObject g_selectedObject;
    public Camera cameraAR;

    public bool g_thrown;
    public bool g_holding;


    bool m_itemSelected;

    bool m_touchDown;
    bool m_touchHold; 
    bool m_touchUp;

    Vector2 m_touchPos;
    float m_touchStartTime;
    float m_touchEndTime;
    float m_swipeTime;

    float m_swipeDistance;
    Vector3 m_startPos;
    Vector3 m_endPos;
    Vector3 m_startPosNorm;
    Vector3 m_endPosNorm;

    Rigidbody m_rigidBodyFood;
    float m_screenDistance;
    Vector3 m_initialPosition;
    Quaternion m_initialRotation;
    Vector3 m_newPosition;
    Vector3 m_force;
    float m_speed;
    float m_finalSpeedClamped;

    public List<GameObject> animalObjectsInSceneList = new List<GameObject>();

    void Start () 
    {
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            TouchInput();
        }
        else
            PCInput();

        if (UIManager.Instance.isAnimalPlaced() && !UIManager.Instance.isStartGameButtonEnabled && FoodGenerator.Instance.g_isTrayReady && UIManager.Instance.allowInput)
        {
            LaunchFood();
        }
       

    }

    void FixedUpdate()
    {
        if (g_thrown)
        {
            AddForce();
        }
    }

    //animal place & position
    public void Deslect()     {         g_selectedObject = null;         m_itemSelected = false;     }

    public void PositionAnimalsAR(GameObject animalPrefab)     {         if (m_touchDown)
        {
           Vector3 spawnedAnimalPosition = CastRayForAnimalPlacement();
           AnimalHandler.Instance.PlaceAnimalFromStash(spawnedAnimalPosition);         }      }

    Vector3 CastRayForAnimalPlacement()     {         Ray ray = cameraAR.ScreenPointToRay(m_touchPos);         float planeY = UnityARGeneratePlane.Instance.returnHeight();         float t = (planeY - ray.origin.y) / ray.direction.y;         float xAnswer = ray.origin.x + t * ray.direction.x;         float zAnswer = ray.origin.z + t * ray.direction.z;         Vector3 answer = new Vector3(xAnswer, planeY, zAnswer);                  return answer;     }          void InstantiateAnimal(GameObject animalPrefab,Vector3 spawnedAnimalPosition)     {         //Spawn animal at position         Vector3 lookPos = cameraAR.transform.position - spawnedAnimalPosition;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        GameObject spawnedAnimal = Instantiate(animalPrefab, spawnedAnimalPosition, rotation);
        //Vector3 cameraXZPos = new Vector3(cameraAR.transform.position.x, spawnedAnimal.transform.position.y, cameraAR.transform.position.z);
        //spawnedAnimal.transform.LookAt(lookPos);         //spawnedAnimal.transform.rotation = rotation;         AnimalHandler.Instance.currentAnimalCount++;         animalObjectsInSceneList.Add(spawnedAnimal);     }
     void SelectObjectAR()     {          if (Input.touchCount > 0 && !IsPointerOverUIObject())         {             Touch touch = Input.GetTouch(0);              if (touch.phase == TouchPhase.Began)             {                  RaycastHit[] hits;                 hits = Physics.RaycastAll(cameraAR.ScreenPointToRay(Input.GetTouch(0).position), Mathf.Infinity);                  for (int i = 0; i < hits.Length; i++)                 {                     RaycastHit hit = hits[i];                      if (hit.transform.tag == "Selectables")                     {                         m_itemSelected = true;                         g_selectedObject = hit.transform.gameObject;                         g_selectedObject.GetComponent<Collider>().enabled = false;                         break;                     }                 }              }              if (touch.phase == TouchPhase.Moved)             {                  Ray ray = cameraAR.ScreenPointToRay(touch.position);

                float planeY = UnityARGeneratePlane.Instance.returnHeight();
                 float t = (planeY - ray.origin.y) / ray.direction.y;                  float xAnswer = ray.origin.x + t * ray.direction.x;                 float zAnswer = ray.origin.z + t * ray.direction.z;                  Vector3 answer = new Vector3(xAnswer, planeY, zAnswer);                  g_selectedObject.transform.position = answer;              }             else if (touch.phase == TouchPhase.Ended)             {                 g_selectedObject.GetComponent<Collider>().enabled = true;                 Deslect();             }         }     } 
    //other
    public bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        List<RaycastResult> ignoreLayers = new List<RaycastResult>();
        foreach (RaycastResult res in results)
        {

            if (res.gameObject.layer != 2)
            {
                ignoreLayers.Add(res);
            }

        }
        return ignoreLayers.Count > 0;
    }

    //FOOD
    void LaunchFood()
    {

        GameObject foodTobeLaunched = FoodGenerator.Instance.m_Tray[0];
        m_rigidBodyFood = foodTobeLaunched.GetComponent<Rigidbody>();
        m_initialPosition = foodTobeLaunched.transform.position;
        m_initialRotation = foodTobeLaunched.transform.rotation;

        if (g_holding )
        {
            OnTouchHoldFood();
        }

        if (m_touchDown)
         {
            CastRayForFoodLaunch(foodTobeLaunched);
         }
         else if (m_touchUp && g_holding)
         {
            OnTouchUp();
         }

    }

    void CastRayForFoodLaunch(GameObject foodToLaunch)
    {
        Ray ray = cameraAR.ScreenPointToRay(m_touchPos);
        RaycastHit hit;

        //Cast a ray from the camera
        if (Physics.Raycast(ray, out hit, 100f))
        {
            //If what we hit is the cubes's tranform
            if (hit.transform == foodToLaunch.transform)
            {
                FoodGenerator.Instance.g_isLaunching = true;
                m_rigidBodyFood.isKinematic = false;
                foodToLaunch.GetComponentInChildren<FloatingObject>().enabled = false;
                m_touchStartTime = Time.time;
                m_startPos = m_touchPos;
                g_holding = true;
            }
        }
    }

    void OnTouchUp()
    {
        m_touchEndTime = Time.time;
        m_endPos = m_touchPos;


        CalculateForce();

        m_swipeTime = m_touchEndTime - m_touchStartTime;
        m_swipeDistance = (m_endPos - m_startPos).magnitude;
        if (m_swipeTime < 0.5f && m_swipeDistance > 0.5f)
        {
            g_holding = false;
            g_thrown = true;
            StartCoroutine(AddtoListWithDelay());
        }
        else
        {
            Reset();
        }
    }


    void AddForce()
    {
        GameObject foodToBeFired = FoodGenerator.Instance.m_Tray[0];
        foodToBeFired.transform.rotation = cameraAR.transform.rotation;
        m_rigidBodyFood.AddRelativeForce(m_force * m_finalSpeedClamped);
        foodToBeFired.GetComponent<FoodBehaviour>().inTransition = true;
        foodToBeFired.GetComponent<FoodBehaviour>().inTray = false;
        foodToBeFired.GetComponent<TimeBasedDestroy>().enabled = true;
        m_rigidBodyFood.useGravity = true;
        foodToBeFired.transform.SetParent(null);
        g_thrown = false;
    }

    void CalculateForce()
    {
        m_screenDistance = Mathf.Sqrt(Screen.width * Screen.width + Screen.height * Screen.height);
        m_startPosNorm = m_startPos / m_screenDistance;
        m_endPosNorm = m_endPos / m_screenDistance;
        Vector3 screenVector = m_endPosNorm - m_startPosNorm; //screen space
        screenVector.y *= 2;
        m_speed = screenVector.magnitude / m_swipeTime;
        m_finalSpeedClamped = Mathf.Clamp(m_speed, 1.5f, 4) * 100;
        Vector3 offset = new Vector3(0, 0, 1); //world space
        Vector3 worldSpaceDirection = (offset + screenVector);
        worldSpaceDirection.Normalize();
        m_force = worldSpaceDirection;
    }

    IEnumerator AddtoListWithDelay()
    {
        yield return new WaitForSeconds(0.1f);
        if (FoodGenerator.Instance.m_Tray.Count > 0)
        {
            FoodGenerator.Instance.m_Tray.RemoveAt(0);
            FoodGenerator.Instance.addToList();
        }
        FoodGenerator.Instance.g_isLaunching = false;
    }

    void OnTouchHoldFood()
    {
        FoodGenerator.Instance.m_Tray[0].transform.SetParent(null);
        Ray ray = cameraAR.ScreenPointToRay(m_touchPos);
        FoodGenerator.Instance.m_Tray[0].transform.position = ray.origin;
        FoodGenerator.Instance.m_Tray[0].transform.rotation = FoodGenerator.Instance.m_SpawnPos.rotation;
        FoodGenerator.Instance.m_Tray[0].transform.SetParent(cameraAR.transform);

    }

    //Reset first item if not launched
    private void Reset()
    {
        Transform spawnPoint = FoodGenerator.Instance.m_SpawnPos.transform;
        FoodGenerator.Instance.m_Tray[0].transform.SetPositionAndRotation(spawnPoint.position, spawnPoint.rotation);
        m_rigidBodyFood.velocity = Vector3.zero;
        m_rigidBodyFood.angularVelocity = Vector3.zero;
        m_rigidBodyFood.useGravity = false;
        FoodGenerator.Instance.m_Tray[0].GetComponentInChildren<FloatingObject>().enabled = true;
        FoodGenerator.Instance.m_Tray[0].GetComponent<Rigidbody>().isKinematic = true;
        g_thrown = g_holding = false;

    }

    void TouchInput()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            m_touchDown = touch.phase ==  TouchPhase.Began;
            m_touchHold = touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary;
            m_touchUp = touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended;
            m_touchPos = Input.GetTouch(0).position;
        }
    }


    void PCInput()
    {
        m_touchDown = Input.GetMouseButtonDown(0);
        m_touchHold = Input.GetMouseButton(0);
        m_touchUp = Input.GetMouseButtonUp(0);
        m_touchPos = Input.mousePosition;
    }


}
