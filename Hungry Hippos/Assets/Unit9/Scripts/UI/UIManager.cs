﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour {

    //Note: use listeners in code, easier to track down.

    static UIManager uIManager;
    public static UIManager Instance
    {
        get
        {
            if (uIManager == null)
                uIManager = GameObject.FindObjectOfType<UIManager>();

            return uIManager;
        }
    }

    //canvas
    public GameObject mainMenuCanvas;
    public GameObject splashScreenCanvas;
    public GameObject arSceneSetupCanvas;
    public GameObject HUDCanvas;
    public GameObject GameOverCanvas;
    public GameObject CountDownCanvas;

    //Text
    public Text debugText;
    public InputField debugInputField,debugInputField2;
    public Text scoreText;
    public Text timerText;
    public Text finalScoreText;
    public Text countDownText;
    bool countDownEnded;
    //Buttons
    public GameObject playAgainButton;
    public GameObject startGameButton;
    public bool isStartGameButtonEnabled ;

    public bool allowInput;

    GameController gameController;
   
    //Food tray
    public GameObject foodTray;

    void Start () 
    {
        TurnOffThings();
        StartCoroutine(SplashScreen());
        isStartGameButtonEnabled = true;
        gameController = GameController.Instance;
        allowInput = false;
    }

	void Update () 
    {
        if (isAnimalPlaced() && isStartGameButtonEnabled)
        {
            startGameButton.SetActive(true);
        }
    }

    //other
    void TurnOffThings()
    {
        splashScreenCanvas.SetActive(true);
        mainMenuCanvas.SetActive(false);
        arSceneSetupCanvas.SetActive(false);
        HUDCanvas.SetActive(false);
    }

    //buttons
    public void PlayGame()
    {
        mainMenuCanvas.SetActive(false);
        arSceneSetupCanvas.SetActive(true);
        gameController.currentState = GameController.GameStates.animalPlacement;
        ARSetUpScript.Instance.StartScanSurroundings();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        StartCoroutine(CountDown(3));
    }

    public void PlayAgain()
    {
        GameOverCanvas.SetActive(false);
        AnimalHandler.Instance.g_repeatGame = true;
        AnimalHandler.Instance.isAnimalToSpawnRegenerated = false;
        FoodGenerator.Instance.g_isTrayRegenarated = false;
        FoodGenerator.Instance.g_isTrayReady = false;
        mainMenuCanvas.SetActive(true);
        gameController.currentState = GameController.GameStates.MainMenu;
        isStartGameButtonEnabled = true;

    }

    public bool isAnimalPlaced()
    {
        if (AnimalHandler.Instance.currentAnimalCount == AnimalHandler.Instance.maxNumAnimals)
        {
            return true;
        }
        return false;
    }

    IEnumerator SplashScreen()
    {
        yield return new WaitForSeconds(5f);
        splashScreenCanvas.SetActive(false);
        mainMenuCanvas.SetActive(true);
        gameController.currentState = GameController.GameStates.MainMenu;
    }

    IEnumerator CountDown(int seconds)
    {
        startGameButton.SetActive(false);
        isStartGameButtonEnabled = false;
        foodTray.SetActive(true);
        gameController.score = 0;
        gameController.timer = 30;
        HUDCanvas.SetActive(true);
        ARSetUpScript.Instance.instructionText.enabled = false;
        gameController.currentState = GameController.GameStates.GameAR;

        int count = seconds;

        while (count > 0)
        {
            countDownText.text = count.ToString();
            CountDownCanvas.SetActive(true);
            yield return new WaitForSeconds(0.9f);
            CountDownCanvas.SetActive(false);
            count--;
        }
        CountDownCanvas.SetActive(true);
        countDownText.text = "Go!";
        allowInput = true;
        yield return new WaitForSeconds(0.9f);
        CountDownCanvas.SetActive(false);
    }

}
