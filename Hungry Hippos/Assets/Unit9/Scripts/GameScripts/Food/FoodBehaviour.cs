﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBehaviour : MonoBehaviour {

    public foodtypes currentFood;
    public bool inTray;
    public bool inTransition;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ApplyFood(foodtypes ft, bool it, bool itransit)
    {
        currentFood = ft;
        inTray = it;
        inTransition = itransit;
    }


}
