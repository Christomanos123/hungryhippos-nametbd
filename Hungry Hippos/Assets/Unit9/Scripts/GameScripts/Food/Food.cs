﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum foodtypes { icecream, burger,rotten };

public class Food  
{


    public static foodtypes GetRandomFoodtype()
    {
        return (foodtypes)Random.Range(0, 3);
    }

    foodtypes currentFood;
    bool inTray;
    GameObject prefab;

    public Food(foodtypes ft, bool t, GameObject p)
    {
        currentFood = ft;
        inTray = t;
        prefab = p;
    }
}
