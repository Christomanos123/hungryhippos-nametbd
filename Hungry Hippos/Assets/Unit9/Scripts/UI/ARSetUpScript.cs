﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ARSetUpScript : MonoBehaviour {


    static ARSetUpScript aRSetUp;
    public static ARSetUpScript Instance
    {
        get
        {
            if (aRSetUp == null)
                aRSetUp = GameObject.FindObjectOfType<ARSetUpScript>();

            return aRSetUp;
        }
    }

    public Text instructionText;

    bool waitingForTap;
    bool scanComplete;
    public  bool hasRun;


    // Use this for initialization
    void Start () 
    {
        hasRun = false;
	}

	void Update () 
    {
	}

    IEnumerator WaitingForScanComplete()     {
        Debug.Log("looping update");         while (waitingForTap)         {

             if (Input.touchCount > 0 || Input.GetKey(KeyCode.A))              {
                Debug.Log("touching screen");

                scanComplete = true;
                waitingForTap = false;             }
            yield return null;         }     }

    public void StartScanSurroundings()
    {
        if(GameController.Instance.currentState == GameController.GameStates.animalPlacement)
            StartCoroutine(ScanSurroundings());
    }
    IEnumerator ScanSurroundings()
    {
       
        hasRun = true;
        yield return new WaitForSeconds(2);

        waitingForTap = true;
        instructionText.enabled = true;
        instructionText.text = "Scan the surrounding Area. Tap to complete Scan";

        yield return StartCoroutine(WaitingForScanComplete());
        //WaitingForScanComplete();


        while (!scanComplete)
            yield return null;


        AnimalHandler.Instance.placingAnimals = true;

        instructionText.text = "Tap to place an animal!";
        
    


    } 
}
