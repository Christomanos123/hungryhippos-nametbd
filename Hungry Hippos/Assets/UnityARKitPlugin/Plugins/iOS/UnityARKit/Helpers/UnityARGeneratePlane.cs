﻿using System;
using System.Collections.Generic;

namespace UnityEngine.XR.iOS
{
	public class UnityARGeneratePlane : MonoBehaviour
	{
    	static UnityARGeneratePlane unityARGeneratePlane;         public static UnityARGeneratePlane Instance         {             get             {                 if (unityARGeneratePlane == null)                     unityARGeneratePlane = GameObject.FindObjectOfType<UnityARGeneratePlane>();                  return unityARGeneratePlane;             }         }          public GameObject planePrefab;          UnityARAnchorManager unityARAnchorManager;         List<ARPlaneAnchorGameObject> arpags = new List<ARPlaneAnchorGameObject>();          bool meshOn;        // Use this for initialization         void Start ()          {             unityARAnchorManager = new UnityARAnchorManager();             UnityARUtility.InitializePlanePrefab (planePrefab);              meshOn = true;         }          void OnDestroy()         {             unityARAnchorManager.Destroy ();         }
        /*void OnGUI()     {
  IEnumerable<ARPlaneAnchorGameObject> arpags = unityARAnchorManager.GetCurrentPlaneAnchors ();         foreach(var planeAnchor in arpags)         {                 //ARPlaneAnchor ap = planeAnchor;                 //GUI.Box (new Rect (100, 100, 800, 60), string.Format ("Center: x:{0}, y:{1}, z:{2}", ap.center.x, ap.center.y, ap.center.z));                 //GUI.Box(new Rect(100, 200, 800, 60), string.Format ("Extent: x:{0}, y:{1}, z:{2}", ap.extent.x, ap.extent.y, ap.extent.z));         }     }*/

        public float returnHeight()         {             LinkedList<ARPlaneAnchorGameObject> temp = unityARAnchorManager.GetCurrentPlaneAnchors();             arpags = new List<ARPlaneAnchorGameObject>();              foreach (ARPlaneAnchorGameObject t in temp)                 arpags.Add(t);


            float lowestAnchorY = 0f;

            for (int i = 0; i < arpags.Count; i++)
            {
                if (i == 0)                     lowestAnchorY = arpags[0].gameObject.transform.position.y;
                else if (arpags[i].gameObject.transform.position.y < lowestAnchorY)
                    lowestAnchorY = arpags[i].gameObject.transform.position.y;             }

            return lowestAnchorY;
        
        }

        public void addColliderToMesh()
        {
            LinkedList<ARPlaneAnchorGameObject> temp = unityARAnchorManager.GetCurrentPlaneAnchors();
            arpags = new List<ARPlaneAnchorGameObject>();

            foreach (ARPlaneAnchorGameObject t in temp)
                arpags.Add(t);


            for (int i = 0; i < arpags.Count; i++)
            {
                arpags[i].gameObject.AddComponent<MeshCollider>();
            }
        }

        public void ToggleMesh()
        {
            LinkedList<ARPlaneAnchorGameObject> temp = unityARAnchorManager.GetCurrentPlaneAnchors();
            arpags = new List<ARPlaneAnchorGameObject>();

            foreach (ARPlaneAnchorGameObject t in temp)
                arpags.Add(t);

            /*if (meshOn)
            {
                foreach (ARPlaneAnchorGameObject anchor in arpags)
                {
                    anchor.gameObject.SetActive(false);
                }
            }
            else
            {
                foreach (ARPlaneAnchorGameObject anchor in arpags)
                {
                    anchor.gameObject.SetActive(true);
                }
            }*/

            foreach (ARPlaneAnchorGameObject anchor in arpags)
            {
                anchor.gameObject.SetActive(!meshOn);
            }
        }
     public void TurnOffRenderer()     {         LinkedList<ARPlaneAnchorGameObject> temp = unityARAnchorManager.GetCurrentPlaneAnchors();         arpags = new List<ARPlaneAnchorGameObject>();          foreach (ARPlaneAnchorGameObject t in temp)             arpags.Add(t);       foreach (ARPlaneAnchorGameObject anchor in arpags)          {            anchor.gameObject.GetComponentInChildren<MeshRenderer> ().enabled = false;             anchor.gameObject.GetComponentInChildren<LineRenderer>().enabled = false;         }       }
     public void TurnOnRenderer()     {         LinkedList<ARPlaneAnchorGameObject> temp = unityARAnchorManager.GetCurrentPlaneAnchors();         arpags = new List<ARPlaneAnchorGameObject>();          foreach (ARPlaneAnchorGameObject t in temp)             arpags.Add(t);       foreach (ARPlaneAnchorGameObject anchor in arpags)          {            anchor.gameObject.GetComponentInChildren<MeshRenderer> ().enabled = true;             anchor.gameObject.GetComponentInChildren<LineRenderer>().enabled = true;         }     }    }
}

