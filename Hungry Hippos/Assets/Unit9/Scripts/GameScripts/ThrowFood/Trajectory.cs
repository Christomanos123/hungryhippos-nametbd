﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Trajectory : MonoBehaviour {


    [SerializeField]private Transform TargetObjectTF;
    [SerializeField] private float LaunchAngle;


    private Rigidbody rigid;
    private Vector3 initialPosition;
    private Quaternion initialRotation;

    //[SerializeField] private float force;
    //[SerializeField] private ForceMode forceMode;
    //rigid.AddForce(transform.forward * force, forceMode);

   
    void Start ()
    {
        rigid = GetComponent<Rigidbody>();
        initialPosition = transform.position;
        initialRotation = transform.rotation;
	}
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reset();
        }
	}

    private void Reset()
    {
        rigid.velocity = Vector3.zero;
        transform.SetPositionAndRotation(initialPosition, initialRotation);
    }


   public void LaunchWithTarget()
    {
        Vector3 projectileXZPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Vector3 targetXZPos = new Vector3(TargetObjectTF.position.x, transform.position.y, TargetObjectTF.position.z);

        transform.LookAt(targetXZPos);

        float R = Vector3.Distance(projectileXZPos, targetXZPos);
        float G = Physics.gravity.y;
        float tanAlpha = Mathf.Tan(LaunchAngle * Mathf.Deg2Rad);
        float H = TargetObjectTF.position.y - transform.position.y;

        float Vz = Mathf.Sqrt(G * R * R / (2.0f * (H - R * tanAlpha)));
        float Vy = tanAlpha * Vz;

        Vector3 localVelocity = new Vector3(0f, Vy, Vz);

        Vector3 globalVelocity = transform.TransformDirection(localVelocity);

        rigid.velocity = globalVelocity;
       // UpdateTrajectory(projectileXZPos, globalVelocity, Physics.gravity);
    }

     Vector3 velocity;
    [SerializeField] float velocityAmount;

    public void Launch()
    {
        velocity.z = velocityAmount * Mathf.Cos(LaunchAngle * Mathf.Deg2Rad);
        velocity.y = velocityAmount * Mathf.Sin(LaunchAngle * Mathf.Deg2Rad);
        rigid.velocity = velocity;
        //UpdateTrajectory(transform.position, velocity, Physics.gravity);
    }

    void UpdateTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity)
    {
        int numSteps = 40;
        float timeDelta = 1.0f / initialVelocity.magnitude; 

        LineRenderer lineRenderer = GetComponent<LineRenderer>();
         
        lineRenderer.positionCount = numSteps;

        Vector3 position = initialPosition;
        Vector3 velocity = initialVelocity;
        for (int i = 0; i < numSteps; ++i)
        {
            lineRenderer.SetPosition(i, position);

            position += velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta;
            velocity += gravity * timeDelta;
        }
    }


}
