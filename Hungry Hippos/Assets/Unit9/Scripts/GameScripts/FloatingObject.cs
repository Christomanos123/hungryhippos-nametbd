﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingObject : MonoBehaviour {

    Vector3 startPos;

    [SerializeField] private float amplitude;
    [SerializeField] private float period;

    protected void Start()
    {
        //startPos = FoodGenerator.Instance.m_SpawnPos.position;
    }

    protected void Update()
    {
        startPos = FoodGenerator.Instance.m_SpawnPos.position;
        float theta = Time.timeSinceLevelLoad / period;
        float distance = amplitude * Mathf.Sin(theta);
        transform.position = startPos + Vector3.up * distance;
    }
}
