﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum animalTypes { rubish, bear, elephant };

public class Animal 
{

    public static animalTypes GetRandomAnimaltype()
    {
        return (animalTypes)UnityEngine.Random.Range(0,Enum.GetValues(typeof(animalTypes)).Length);
    }

    public animalTypes currentAnimal;
    public GameObject prefab;
    public foodtypes desiredFood;
    public bool inScene;

    public Animal(animalTypes at, GameObject p,foodtypes f,bool s)
    {
        currentAnimal = at;
        prefab = p;
        desiredFood = f;
        inScene = s;
    }

}

