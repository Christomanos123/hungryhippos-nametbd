﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AnimalHandler : MonoBehaviour {

    static AnimalHandler animalHandler;
    public static AnimalHandler Instance
    {
        get
        {
            if (animalHandler == null)
                animalHandler = GameObject.FindObjectOfType<AnimalHandler>();

            return animalHandler;
        }
    }
    public GameObject cameraAr;

    public int maxNumAnimals;
    public int currentAnimalCount;

    public bool g_repeatGame;
    public bool placingAnimals;

    public GameObject bearPrefab;
    public GameObject elephantPrefab;
    public GameObject trashCanPrefab;

    public Animal animalToSpawn;
    bool hasRun;
    public bool isAnimalToSpawnRegenerated;
    public List<Animal> animalList = new List<Animal>();
    public List<Animal> spawnedAnimalList = new List<Animal>();

    public Transform restOfAnimalsPos;

    public List<GameObject> bearStashList = new List<GameObject>();
    public List<GameObject> elephantStashList = new List<GameObject>();
    public List<GameObject> rubishStashList = new List<GameObject>();


    // Use this for initialization
    void Start () 
    {
        g_repeatGame = false;
        isAnimalToSpawnRegenerated = true;
        maxNumAnimals = 1;
        generateAnimalList();
        generateAnimalToSpawn();
        currentAnimalCount = 0;

        for (int i = 0; i < animalList.Count; i++)
        {
            switch (i)
            {
                case 0:
                    InstantiateAnimalStash(rubishStashList, animalList[0].prefab);
                    break;
                case 1:
                    InstantiateAnimalStash(bearStashList, animalList[1].prefab);
                    break;
                case 2:
                    InstantiateAnimalStash(elephantStashList, animalList[2].prefab);
                    break;
                default:
                    break;
            }
        }


    }
	
	// Update is called once per frame
	void Update () 
    {
        if (currentAnimalCount == maxNumAnimals)
            placingAnimals = false;

        if (GameController.Instance.currentState == GameController.GameStates.animalPlacement && g_repeatGame && isAnimalToSpawnRegenerated == false)
        {
            generateAnimalToSpawn();
            isAnimalToSpawnRegenerated = true;
        }

        if(placingAnimals)
        {
            TouchControls.Instance.PositionAnimalsAR(animalToSpawn.prefab);
        }
    }

    void generateAnimalToSpawn()
    {
       animalTypes randomEnum = Animal.GetRandomAnimaltype();

        if(!spawnedAnimalList.Any(i => i.currentAnimal == randomEnum))
        {
            for (int i = 0; i< animalList.Count; i++)
            {
                if (animalList[i].currentAnimal == randomEnum)
                {
                    spawnedAnimalList.Add(animalList[i]);
                    animalToSpawn = animalList[i];
                }
            }
        }
    }


    public void PlaceAnimalFromStash(Vector3 posAR)
    {
        for (int spawnAnimalCount = 0; spawnAnimalCount < spawnedAnimalList.Count; spawnAnimalCount++)
        {

            switch (spawnedAnimalList[spawnAnimalCount].currentAnimal)
            {
                case animalTypes.bear:
                    for (int bearStashCount = 0; bearStashCount < bearStashList.Count; bearStashCount++)
                    {
                        if (bearStashList[bearStashCount].GetComponent<AnimalBehaviour>().inScene == false && currentAnimalCount != maxNumAnimals)
                        {
                            bearStashList[bearStashCount].transform.SetParent(null);
                            bearStashList[bearStashCount].transform.position = posAR;
                            bearStashList[bearStashCount].GetComponent<AnimalBehaviour>().inScene = true;
                            currentAnimalCount++;
                        }
                    }
                    break;
                case animalTypes.elephant:
                    for (int elephantStashCount = 0; elephantStashCount < elephantStashList.Count; elephantStashCount++)
                    {
                        if (elephantStashList[elephantStashCount].GetComponent<AnimalBehaviour>().inScene == false && currentAnimalCount != maxNumAnimals)
                        {
                            elephantStashList[elephantStashCount].transform.SetParent(null);
                            elephantStashList[elephantStashCount].transform.position = posAR;
                            elephantStashList[elephantStashCount].GetComponent<AnimalBehaviour>().inScene = true;
                            currentAnimalCount++;
                        }

                    }
                    break;
                case animalTypes.rubish:
                    for (int rubishStashCount = 0; rubishStashCount < rubishStashList.Count; rubishStashCount++)
                    {
                        if (rubishStashList[rubishStashCount].GetComponent<AnimalBehaviour>().inScene == false && currentAnimalCount != maxNumAnimals)
                        {
                            rubishStashList[rubishStashCount].transform.SetParent(null);
                            rubishStashList[rubishStashCount].transform.position = posAR;
                            rubishStashList[rubishStashCount].GetComponent<AnimalBehaviour>().inScene = true;
                            currentAnimalCount++;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }


    void InstantiateAnimalStash(List<GameObject> animalStashlList,GameObject animalPrefab)
    {
        for (int i = 0; i < maxNumAnimals + 1; i++)
        {
            GameObject spawnedAnimal = Instantiate(animalPrefab, restOfAnimalsPos.position, restOfAnimalsPos.rotation);
            spawnedAnimal.transform.SetParent(cameraAr.transform);
            //spawnedAnimal.SetActive(false);
            animalStashlList.Add(spawnedAnimal);
        }
    }


    void generateAnimalList()
    {
        Animal trashCan = new Animal(animalTypes.rubish, trashCanPrefab, foodtypes.rotten,false);
        animalList.Add(trashCan);

        Animal bear = new Animal(animalTypes.bear, bearPrefab, foodtypes.burger,false);
        animalList.Add(bear);

        Animal elephant = new Animal(animalTypes.elephant, elephantPrefab, foodtypes.icecream,false);
        animalList.Add(elephant);
    }

  


    public void Reset()
    {
        spawnedAnimalList.Clear();
        currentAnimalCount = 0;

        for (int bearStashCount = 0; bearStashCount < bearStashList.Count; bearStashCount++)
        {
            bearStashList[bearStashCount].transform.SetParent(TouchControls.Instance.cameraAR.transform);
            bearStashList[bearStashCount].transform.position = restOfAnimalsPos.position;
            bearStashList[bearStashCount].GetComponent<AnimalBehaviour>().inScene = false;
        }

        for (int elephantStashCount = 0; elephantStashCount < elephantStashList.Count; elephantStashCount++)
        {
            elephantStashList[elephantStashCount].transform.SetParent(TouchControls.Instance.cameraAR.transform);
            elephantStashList[elephantStashCount].transform.position = restOfAnimalsPos.position;
            elephantStashList[elephantStashCount].GetComponent<AnimalBehaviour>().inScene = false;
        }

        for (int rubishStashCount = 0; rubishStashCount < rubishStashList.Count; rubishStashCount++)
        {
            rubishStashList[rubishStashCount].transform.SetParent(TouchControls.Instance.cameraAR.transform);
            rubishStashList[rubishStashCount].transform.position = restOfAnimalsPos.position;
            rubishStashList[rubishStashCount].GetComponent<AnimalBehaviour>().inScene = false;
        }



    }


}
