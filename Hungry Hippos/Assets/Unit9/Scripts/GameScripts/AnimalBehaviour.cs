﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalBehaviour : MonoBehaviour {

    public foodtypes desiredFood;
    public animalTypes currentAnimal;
    public bool inScene;

    public SkinnedMeshRenderer materialColour;
    float startTime;
   
    public float fadeOutTime = 1;

    public void ApplyAnimal(animalTypes at, foodtypes f,bool s)
    {
        currentAnimal = at;
        desiredFood = f;
        inScene = s;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Check to see if we collided with some food
        if (other.gameObject.CompareTag("Food"))
        {
            foodtypes ft = other.gameObject.GetComponent<FoodBehaviour>().currentFood;
            //Check if the food is the same food type as its desired food
            /*if (ft == desiredFood )
            {
                GameController.Instance.score += 100;
                StartCoroutine(GreenColourFadeOut());
                other.GetComponent<FoodBehaviour>().inTransition = false;

                other.GetComponent<FoodBehaviour>().inTray = false;
                other.GetComponent<TimeBasedDestroy>().enabled = false;
            }
            else
            {
                GameController.Instance.score -= 50;
                StartCoroutine(RedColourFadeOut());
                if (GameController.Instance.score == 0)
                {
                    GameController.Instance.score = 0;
                }
                other.GetComponent<FoodBehaviour>().inTransition = false;

                other.GetComponent<FoodBehaviour>().inTray = false;
                other.GetComponent<TimeBasedDestroy>().enabled = false;
            }*/
            bool toggle = (ft == desiredFood);
            DisposeFood(other, toggle ? 100 : -50, toggle ? Color.green : Color.red);
        }
    }

    private void DisposeFood(Collider other, int score,Color color)
    {
        GameController.Instance.score += score;
        StartCoroutine(ColourFadeOut(color));
        if (GameController.Instance.score == 0)
        {
            GameController.Instance.score = 0;
        }
        other.GetComponent<FoodBehaviour>().inTransition = false;

        other.GetComponent<FoodBehaviour>().inTray = false;
        other.GetComponent<TimeBasedDestroy>().enabled = false;
    }

    IEnumerator ColourFadeOut(Color color)
    {
        for (float t = 0.01f; t < fadeOutTime; t += 0.1f)
        {
            materialColour.material.color = Color.Lerp(color, Color.grey, t / fadeOutTime);
            yield return null;
        }
    }

    /*IEnumerator GreenColourFadeOut()
    {
        for (float t = 0.01f; t < fadeOutTime; t += 0.1f)
        {
            materialColour.material.color = Color.Lerp(Color.green, Color.grey, t/fadeOutTime);
            yield return null;
        }
    }

    IEnumerator RedColourFadeOut()
    {
        for (float t = 0.01f; t < fadeOutTime; t += 0.1f)
        {
            materialColour.material.color = Color.Lerp(Color.red, Color.grey, t / fadeOutTime);
            yield return null;
        }
    }*/

}
