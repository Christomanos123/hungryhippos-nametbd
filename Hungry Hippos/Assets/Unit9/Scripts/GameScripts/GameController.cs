﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    //GamesStates
    public enum GameStates {MainMenu, animalPlacement,GameAR, GameOver};
    public GameStates currentState;

    public float timer = 30;
    public int score = 0;

    static GameController gameController;
    public static GameController Instance
    {
        get
        {
            if (gameController == null)
            {
                gameController = FindObjectOfType<GameController>();
            }

            return gameController;
        }
    }

    // Use this for initialization
    void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        Debug.Log("CurrentState: " + currentState);

        if (currentState == GameStates.GameAR)
        {
           SetTimer();
           UIManager.Instance.scoreText.text = "Score : " + score.ToString();
        }
        else if (currentState == GameStates.GameOver)
        {
             UIManager.Instance.GameOverCanvas.SetActive(true);
             AnimalHandler.Instance.Reset();
             FoodGenerator.Instance.Reset();
             TouchControls.Instance.g_holding = false;
             UIManager.Instance.finalScoreText.text = "Score: " + score.ToString();
             UIManager.Instance.HUDCanvas.SetActive(false);
        }

	}


    void SetTimer()
    {

        if (timer > 0.00f)
        {
            timer -= Time.deltaTime;
            UIManager.Instance.timerText.text = "Time : " + timer.ToString("F2");
            
        }
        else
        {
            timer = 0.00f;
            UIManager.Instance.timerText.text = "Time : " + timer.ToString("F2");
            currentState = GameStates.GameOver;
        }

    }

}
